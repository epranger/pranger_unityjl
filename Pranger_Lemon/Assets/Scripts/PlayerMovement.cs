﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    
    public float turnSpeed = 20f;

    Animator m_animator;
    Rigidbody m_rigidbody;
    Vector3 m_movement;
    Quaternion m_rotation = Quaternion.identity;
    

    // Start is called before the first frame update
    void Start()
    {

        m_animator = GetComponent<Animator>();
        m_rigidbody = GetComponent<Rigidbody>();

    }

    void OnAnimatorMove()
    {

        m_rigidbody.MovePosition(m_rigidbody.position + m_movement * m_animator.deltaPosition.magnitude);
        m_rigidbody.MoveRotation(m_rotation);

    }

    // Update is called once per frame
    void FixedUpdate()
    {

        float horizontal = Input.GetAxis("Horizontal");
        float vertical = Input.GetAxis("Vertical");
        

        m_movement.Set(horizontal, 0f, vertical);
        m_movement.Normalize();

        bool hasHorizontalInput = !Mathf.Approximately(horizontal, 0f);
        bool hasVerticalInput = !Mathf.Approximately(vertical, 0f);
        bool isWalking = hasHorizontalInput || hasVerticalInput;
        m_animator.SetBool("IsWalking", isWalking);

        Vector3 desiredForward = Vector3.RotateTowards(transform.forward, m_movement, turnSpeed * Time.deltaTime, 0f);
        m_rotation = Quaternion.LookRotation(desiredForward);
    }
}
